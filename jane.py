def num(s, pos):
    return int(s[pos])

def add(s):
    return chains(s)

def minus(s):
    return chains(s)

def mul(s):
    return chains(s)

def div(s):
    return chains(s)

def apply_mix(val, s, pos):
    if s[pos] == '+':
        val += num(s, pos + 1)
    elif s[pos] == '-':
        val -= num(s, pos + 1)  
    if s[pos] == '*':
        val *= num(s, pos + 1)  
    elif s[pos] == '/':
        val /=  num(s, pos + 1)
    return val

def chains(s):
    val = num(s, 0)
    for i in range(1, len(s)):
        val = apply_mix(val, s, i)
    return val

