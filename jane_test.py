import unittest
from jane import *
from expr import *


class ExprTestCase(unittest.TestCase):
    def setUp(self):
        return
    def tearDown(self):
        return

    def test_num_index0(self):
        self.assertEqual(1, num("1", 0))
        self.assertEqual(2, num("2+3", 0))

    def test_num_index2(self):
        self.assertEqual(3, num("2+3", 2))

    def test_add(self):
        self.assertEqual(2, add("1+1"))
        self.assertEqual(3, add("1+2"))

    def test_add3(self):
        self.assertEqual(6, add("1+2+3"))
    
    def test_minus(self):
        self.assertEqual(1, minus("2-1"))
        self.assertEqual(0, minus("2-1-1"))

    def test_mul(self):
        self.assertEqual(6, mul("2*3"))
        self.assertEqual(24, mul("2*3*4"))

    def test_div(self):
        self.assertEqual(2, div("6/3"))
        self.assertEqual(1, div("9/3/3"))


if __name__ == '__main__':
    unittest.main()


